/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  private defaultTab = 'tab1';
  private history: string[] = [this.defaultTab];
  public tabSelected: string = this.defaultTab;

  constructor() {}

  /**
   * Get the navigation history
   * @return [string] the navigation history
   */
  public get() {
    return this.history;
  }

  /**
   * Add a tab to the navigation history, and remove the oldes one if the history is more than 2 tabs long.
   * This history always keep the default tab at the first position.
   * @param tab the tab name to add to the history
   */
  public push(tab: string): void {
    if (tab !== this.currentTab) {
      if (this.history.length > 2) {
        this.history.splice(1, 1); // remove the oldest tab
      }
      if (this.history[1] === this.defaultTab) {
        this.history.splice(1, 1); // remove the default tab if it's the second tab to avoid having it twice
      }
      this.history.push(tab);
    }
  }

  /**
   * Remove the last tab from the history and return it.
   * @return string the removed tab
   */
  public pop(): string | null {
    if (this.history.length > 1) {
      const tab = this.history.pop();
      this.tabSelected = this.currentTab;
      return tab;
    } else {
      return null;
    }
  }

  /**
   * Get the newest tab from the history
   */
  public get currentTab(): string {
    return this.history[this.history.length - 1];
  }

  /**
   * Update the current selected tab.
   */
  updateTabSelected(tab: string) {
    this.tabSelected = tab;
  }
}
