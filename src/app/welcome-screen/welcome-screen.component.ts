/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { LanguageService } from '../services/language.service';
import { StorageService } from '../services/storage.service';
import { TourService } from '../services/tour.service';
import { Pagination } from 'swiper/modules';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import SwiperCore from 'swiper';

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-welcome-screen',
  templateUrl: 'welcome-screen.component.html',
  styleUrls: ['welcome-screen.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WelcomeScreenComponent implements OnInit, OnDestroy {
  selectedLanguage = 'en';
  selectedLanguageSub: Subscription;
  isDarkModeEnable = window.matchMedia('(prefers-color-scheme: dark)').matches; // Use matchMedia to check the user preference

  constructor(
    private languageService: LanguageService,
    private storageService: StorageService,
    private tourService: TourService,
    private router: Router
  ) {}

  ngOnInit() {
    this.selectedLanguageSub = this.storageService.languageSelected.subscribe(
      (lang: string) => {
        this.selectedLanguage = lang;
      }
    );
  }

  start() {
    this.storageService.unLockApp().then(() => {
      this.router.navigate(['/tabs', 'tab1']);
      this.tourService.start();
    });
  }

  onSwitchLanguage(language) {
    this.languageService.setLanguage(language.detail.value);
  }

  ngOnDestroy() {
    this.selectedLanguageSub.unsubscribe();
  }
}
