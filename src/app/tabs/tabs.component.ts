/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { HistoryService } from '../services/history.service';
import { TourService } from '../services/tour.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.component.html',
  styleUrls: ['tabs.component.scss']
})
export class TabsComponent implements OnInit {
  tabSelected;

  constructor(
    private historyService: HistoryService,
    private tourService: TourService,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events.subscribe(() => {
      this.tabSelected = this.router.url;
    });
  }

  clickTab(tab: string) {
    if (this.tourService.isActive()) {
      this.tourService.goTo('addGardenButton');
    }
    this.historyService.updateTabSelected(tab);
    this.historyService.push(tab);
  }
}
