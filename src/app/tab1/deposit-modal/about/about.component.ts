/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Component, Input } from '@angular/core';
import { Vegetable } from '../../../shared/vegetable.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  @Input() vegetable: Vegetable;

  constructor() {}
}
