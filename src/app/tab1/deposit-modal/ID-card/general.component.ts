/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { VegetableService } from '../../../services/vegetable.service';
import { Vegetable } from '../../../shared/vegetable.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  @Input() vegetable: Vegetable;
  @Output() vegetableSelected = new EventEmitter<Vegetable>();

  seasonString = [];

  constructor(
    private modalCtrl: ModalController,
    public alertController: AlertController,
    private vegetableService: VegetableService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.seasonString = this.vegetableService.convertMonthToString(
      this.vegetable.cultureSheets.monthOfHarvest
    );
  }

  async updateModal(vegetableSelected) {
    const vegetable: Vegetable | null =
      this.vegetableService.getVegetableByName(vegetableSelected);

    if (vegetable !== null) {
      this.vegetableSelected.emit(vegetable);
    } else {
      const alert = await this.alertController.create({
        header: this.translate.instant(
          'TAB1.modal.IDCard.errors.vegetableNotAvailable.header'
        ),
        subHeader: this.translate.instant(
          'TAB1.modal.IDCard.errors.vegetableNotAvailable.subHeader'
        ),
        message: this.translate.instant(
          'TAB1.modal.IDCard.errors.vegetableNotAvailable.message'
        ),
        buttons: ['OK']
      });
      await alert.present();
    }
  }
}
