/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { VegetableService } from '../../services/vegetable.service';
import { VegetableMapping } from '../../shared/garden.model';
import { Vegetable } from '../../shared/vegetable.model';
import { Injectable } from '@angular/core';
import { fabric } from 'fabric';
import { TranslateService } from '@ngx-translate/core';

interface TimeLine {
  ['year']: {
    ['month']: { vegetableId: string; zoneName: string }[];
  }[];
}

export interface EvolutionResponse {
  type: string;
  message: string;
}

type Population = Chromosome[];
type Chromosome = Gene[];
type Gene = number;

@Injectable({ providedIn: 'root' })
export class EvolutionService {
  private timeline: TimeLine;
  private zoneNames: string[];
  private vegetablesId: string[];
  private populationSize = 20;
  private generationLimit = 50;
  private zoneMatrix: Array<Array<number>>;
  private plantingSchematics: { ['month']: string[] };
  private vegetablePlacementHistory: { ['gene']: string[] };

  constructor(
    private vegetableService: VegetableService,
    private translate: TranslateService
  ) {}

  /**
   * This function assigns the vegetables in the zones and takes care about
   * favourable/unfavourable association and interval time between each plantation.
   * @param canvasObject The matrix of the zones where 1 means that two zones are connected and 0 for no connection.
   * @param vegetablesId The list of vegetable id to place
   * @param zoneNames The zones names in relation with zoneMatrix
   * @param plantingMonths The different months when a new vegetables is planting
   * @param timelineInterval Interval of years for generating the garden.
   * @author DERACHE Adrien
   */
  runEvolution(
    canvasObject: fabric.Group[],
    vegetablesId: string[],
    zoneNames: string[],
    plantingMonths: number[],
    timelineInterval: { start: number; end: number; interval: () => number }
  ): Promise<[TimeLine, EvolutionResponse]> {
    this.zoneNames = zoneNames.sort();
    this.zoneMatrix = this.generateZoneMatrix(canvasObject);
    this.vegetablesId = vegetablesId;

    return new Promise(resolve => {
      let errorMessage: string;

      // Contain the vegetables to plant month by month.
      this.plantingSchematics = this.vegetableService.getVegetableTimeline(
        plantingMonths,
        vegetablesId
      );

      this.vegetablePlacementHistory = new Object({}) as { ['gene']: string[] };
      vegetablesId.forEach((_gene, index) => {
        this.vegetablePlacementHistory[index] = [];
      });

      this.timeline = this.buildVegetableTimeLine(
        timelineInterval,
        Object.keys(this.plantingSchematics).map(Number)
      );

      for (let year in this.timeline) {
        const staticGene: Set<Gene> = new Set(); // List of gene that should not be switched
        let prevChromosome: Chromosome = null;
        for (let month in this.timeline[year]) {
          if (this.zoneNames.length < this.plantingSchematics[month].length) {
            // Not enough zones available to place all vegetables
            // Will show a message to the user after execution
            errorMessage = 'not_enough_zones';
          }

          const genesAvailable: Gene[] = []; // Extract unique vegetable gene from planting schematics
          this.plantingSchematics[month].forEach((vegetableId: string) => {
            genesAvailable.push(vegetablesId.indexOf(vegetableId));
          });

          let population: Population = this.generatePopulation(
            this.populationSize,
            this.zoneNames.length,
            genesAvailable,
            staticGene,
            prevChromosome
          );
          for (let i = 0; i < this.generationLimit; i++) {
            // Sort by descending order
            population.sort(
              (a: Chromosome, b: Chromosome) =>
                this.fitness(b) - this.fitness(a)
            );

            const nextGeneration: Chromosome[] = population.slice(0, 2); // Takes the two best solutions

            for (let j = 0; j < population.length / 2 - 1; j++) {
              const parents: Population = this.selectionPair(population);
              const tmp: [Chromosome, Chromosome] = this.orderCrossover(
                parents[0],
                parents[1],
                staticGene
              );
              let offspringA: Chromosome = this.mutation(tmp[0], staticGene);
              let offspringB: Chromosome = this.mutation(tmp[1], staticGene);
              nextGeneration.push(offspringA, offspringB);
            }
            population = nextGeneration;
          }
          // Sort by descending order
          population.sort(
            (a: Chromosome, b: Chromosome) => this.fitness(b) - this.fitness(a)
          );

          population[0].forEach((gene: number, zoneId: number) => {
            const vegetable: Vegetable = this.getVegetableFromGene(gene);
            if (vegetable === null || this.zoneNames[zoneId] === undefined) {
              return;
            }
            this.assignVegetableToZone(
              vegetable.id,
              this.zoneNames[zoneId],
              +year,
              +month
            );
            const nextMonth = Object.keys(this.plantingSchematics)[
              Object.keys(this.plantingSchematics).indexOf(month) + 1
            ];

            if (
              this.plantingSchematics[nextMonth] &&
              this.plantingSchematics[nextMonth].includes(vegetable.id)
            ) {
              staticGene.add(gene);
            } else {
              staticGene.delete(gene);
            }

            const lastIndex = this.vegetablePlacementHistory[gene].length;
            if (lastIndex === +year - timelineInterval.start) {
              this.vegetablePlacementHistory[gene].push(this.zoneNames[zoneId]);
            }
          });
          prevChromosome = population[0];

          if (this.hasDuplicates(population[0])) {
            throw new Error(
              'Duplicate values in population after running evolution' +
                population[0]
            );
          }
        }
      }
      let message: string;
      switch (errorMessage) {
        case 'not_enough_zones':
          message = this.translate.instant(
            'TAB2.canvas.gardenErrorVegetablesZonesNumber'
          );
          break;
        default:
          message = 'Success';
      }
      return resolve([
        this.timeline,
        {
          type: errorMessage ?? 'success',
          message
        }
      ]);
    });
  }

  /**
   * Generates a new population of chromosomes with unique genes.
   * @param {number} size - The number of chromosomes to generate (Population size).
   * @param {number} genomeLength - The length of each chromosome.
   * @param {number} genesAvailable - The available genes to assign to the chromosomes.
   * @param staticsGenes - The gene array that should not be switched.
   * @param prevChromosome - Used if staticsGenes is not null.
   * @returns {Population} An array of unique chromosomes with no duplicate genes.
   * @private
   */
  private generatePopulation(
    size: number,
    genomeLength: number,
    genesAvailable: number[],
    staticsGenes?: Set<Gene>,
    prevChromosome?: Chromosome
  ): Population {
    return [
      ...new Set(
        [...Array(size)].map(() =>
          this.generateChromosome(
            genesAvailable,
            genomeLength,
            staticsGenes,
            prevChromosome
          )
        )
      ).values()
    ];
  }

  /**
   * Generates a new chromosome with unique and random genes.
   * @param {number} genesAvailable - The available genes to assign to the chromosome.
   * @param {number} size - The length of the chromosome.
   * @param staticsGenes - The gene array that should not be switched.
   * @param prevChromosome - The previous chromosome to compare with staticsGenes.
   * @returns {Chromosome} A new chromosome with a unique randomized gene sequence.
   * @private
   */
  private generateChromosome(
    genesAvailable: number[],
    size: number,
    staticsGenes?: Set<Gene>,
    prevChromosome?: Chromosome
  ): Chromosome {
    if (staticsGenes && staticsGenes.size > 0 && !prevChromosome) {
      throw new Error('prevGenes must not be null if staticsGenes is not.');
    }
    let emptyZonesIndex = -1; // Unique negative empty zone index
    let chromosome: Chromosome = [...Array(size)]
      .map((_, index) =>
        // gene < 0 => Zone is not assign to a vegetable
        index < genesAvailable.length
          ? genesAvailable[index]
          : emptyZonesIndex--
      )
      .filter(x => (staticsGenes.size > 0 ? !staticsGenes.has(x) : true)) // Remove statics genes
      .sort(() => Math.random() - 0.5);

    const sortedGenes: Gene[] = Array.from(staticsGenes).sort(
      (a, b) => prevChromosome.indexOf(a) - prevChromosome.indexOf(b)
    );

    // Restore statics genes
    if (staticsGenes.size > 0) {
      sortedGenes.forEach((gene: Gene) => {
        chromosome.splice(prevChromosome.indexOf(gene), 0, gene);
      });
    }
    return chromosome;
  }

  /**
   * Calculates the fitness score for a given chromosome.
   * @param {Chromosome} chromosome - The chromosome to calculate the fitness score.
   * @returns {Number} The fitness score for the chromosome.
   */
  private fitness(chromosome: Chromosome): number {
    let score = 0;
    if (chromosome.length <= 1) {
      return score;
    }
    chromosome.forEach((gene, zoneId) => {
      if (gene < 0) {
        // Zone is empty
        return;
      }
      const vegetable: Vegetable = this.getVegetableFromGene(gene);
      if (vegetable === null) {
        return;
      }
      // Get the vegetable's neighbors
      const neighborsId = chromosome.filter((_, zone) =>
        this.areZonesTouched(zoneId, zone)
      );
      neighborsId.forEach(neighborId => {
        // CHECK POINT: 1
        // Check for favorable and unfavorable associations between the current vegetable and its neighbors
        //
        const neighbor: Vegetable = this.getVegetableFromGene(neighborId);
        if (!!neighbor) {
          if (
            neighbor.cultureSheets.favourableAssociation &&
            neighbor.cultureSheets.favourableAssociation.includes(
              vegetable.name
            )
          ) {
            score += 10;
          }
          if (
            neighbor.cultureSheets.unfavourableAssociation &&
            neighbor.cultureSheets.unfavourableAssociation.includes(
              vegetable.name
            )
          ) {
            score -= 10;
          }
        }
      });
      // CHECK POINT: 2
      // Check if the planting time interval is respected
      //
      if (
        vegetable.cultureSheets.plantingInterval !== null &&
        vegetable.cultureSheets.plantingInterval > 0
      ) {
        const index = this.vegetablePlacementHistory[gene].length;
        const interval =
          index - vegetable.cultureSheets.plantingInterval < 0
            ? 0
            : index - vegetable.cultureSheets.plantingInterval;
        const history = this.vegetablePlacementHistory[gene].slice(
          interval,
          index
        );
        if (history.includes(this.zoneNames[zoneId])) {
          score -= 15;
        } else {
          score += 15;
        }
      }
    });
    return score;
  }

  /**
   * Selects two chromosomes from the population to create a pair of parents for reproduction.
   * @param {Population} population - The current population of chromosomes.
   * @returns {Population} An array containing two chromosomes selected as a pair of parents for reproduction.
   * @private
   */
  private selectionPair(population: Population): Population {
    const scores: number[] = population.map(chromosome => {
      return this.fitness(chromosome);
    });
    const parent1 = this.weightedRandomIndex(scores);
    const parent2 = this.weightedRandomIndex(scores);
    return [population[parent1], population[parent2]];
  }

  /**
   * Returns the index of an element randomly selected from an array of weights based on their relative weight values.
   * @param {number[]} scores - An array of weight values for each element in the population.
   * @returns {number} The index of an element randomly selected from the array based on its weight value.
   * @private
   */
  private weightedRandomIndex(scores: number[]): number {
    const totalWeight = scores.reduce((acc, weight) => acc + weight, 0);
    const randomWeight = Math.random() * totalWeight;
    let weightSum = 0;
    for (let i = 0; i < scores.length; i++) {
      weightSum += scores[i];
      if (weightSum >= randomWeight) {
        return i;
      }
    }
    return scores.length - 1;
  }

  /**
   * Perform order crossover on two chromosomes.
   * @description Use Order crossover (OX1). https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm)#Uniform_Crossover_and_Half_Uniform_Crossover
   * @param chromosomeA - The first chromosome.
   * @param chromosomeB - The second chromosome.
   * @param staticGene - Gene that should not be permuted.
   * @returns An array containing two offspring chromosomes after the crossover.
   * @throws An error if the length of chromosomeA and chromosomeB is not the same or if there are duplicate values in an offspring chromosome after the crossover.
   */
  private orderCrossover(
    chromosomeA: Chromosome,
    chromosomeB: Chromosome,
    staticGene: Set<Gene>
  ): [Chromosome, Chromosome] {
    if (chromosomeA.length !== chromosomeB.length) {
      throw new Error('ChromosomeA and chromosomeB must have the same length.');
    }

    const length = chromosomeA.length;
    if (length < 2) {
      return [chromosomeA, chromosomeB];
    }

    const chromosomes: Chromosome[] = [chromosomeA, chromosomeB];

    let offsprings: Chromosome[] = new Array(2);
    for (let i = 0; i < 2; i++) {
      offsprings[i] = new Array(length);

      const missingGene: Gene[] = [];
      for (let y = 0; y < length; y++) {
        if (staticGene.has(chromosomes[i][y]))
          offsprings[i][y] = chromosomes[i][y];
        else missingGene.push(chromosomes[i][y]);
      }
      missingGene.sort(
        (a, b) =>
          chromosomes[Math.abs(i - 1)].indexOf(a) -
          chromosomes[Math.abs(i - 1)].indexOf(b)
      );

      for (let y = 0; y < offsprings[i].length; y++) {
        if (!Number.isInteger(offsprings[i][y])) {
          offsprings[i][y] = missingGene.shift();
        }
      }

      if (this.hasDuplicates(offsprings[i])) {
        throw new Error(
          'Duplicate values in offspring after crossover ' +
            offsprings[i].filter(x => x >= 0)
        );
      }
    }

    return [offsprings[0], offsprings[1]];
  }

  /**
   * Perform mutation operation on a chromosome by swapping random genes.
   * @param chromosome - The chromosome to mutate.
   * @param num - The number of mutations to perform (default is 5).
   * @param probability - The probability of each gene being mutated (default is 0.4).
   * @param staticGene - Array of genes that should not be switched
   * @returns The mutated chromosome.
   * @throws Error if duplicates are found in the offspring chromosome.
   */
  private mutation(
    chromosome: Chromosome,
    staticGene: Set<Gene>,
    num: number = 5,
    probability: number = 0.4
  ): Chromosome {
    for (let i = 0; i < num; i++) {
      if (Math.random() <= probability) {
        const index: number =
          Math.floor(Math.random() * (chromosome.length - 1)) + 1;
        if (staticGene.has(chromosome[index])) {
          continue;
        }
        let y = i;
        while (staticGene.has(chromosome[y])) {
          y++;
          if (y >= chromosome.length) {
            break;
          }
        }
        if (staticGene.has(chromosome[y]) || chromosome[y] === undefined) {
          continue;
        }
        const tmpChromosome = chromosome[index];
        chromosome[index] = chromosome[y];
        chromosome[y] = tmpChromosome;
        if (this.hasDuplicates(chromosome)) {
          throw new Error(
            'Duplicate values in offspring after mutation ' + chromosome
          );
        }
      }
    }
    return chromosome;
  }

  /**
   * Assign vegetable to a zone.
   * @param vegetableId - The vegetable ID
   * @param zoneName - The zone name.
   * @param year - The year when to assign vegetable.
   * @param month - The month when to assign vegetable.
   */
  private assignVegetableToZone(
    vegetableId: string,
    zoneName: string,
    year: number,
    month: number
  ) {
    // Check if the zoneName is valid
    if (zoneName === undefined) {
      throw new Error('The zone name is not valid: ' + zoneName);
    }

    this.timeline[year][month].push(
      new VegetableMapping(vegetableId, zoneName)
    );
  }

  /**
   * Given a gene, returns the corresponding Vegetable object.
   * @param gene - The id of the vegetable in the gene.
   * @returns {Vegetable} The plant object corresponding to the gene.
   */
  private getVegetableFromGene(gene: number): Vegetable {
    if (gene < 0 || gene === null || gene === undefined) {
      return null;
    }
    return this.vegetableService.getVegetableById(this.vegetablesId[gene]);
  }

  /**
   * Checks if an array contains duplicate numbers.
   *
   * @param {Array<number>} array - The input array to check.
   * @return {boolean} - True if the array contains duplicate numbers, false otherwise.
   */
  private hasDuplicates(array: Array<number>): boolean {
    const newArr = array.filter(x => x >= 0); // Remove empty zone
    return new Set(newArr).size !== newArr.length;
  }

  /**
   * Returns true if two areas are touching.
   * @param zoneA The zone A index in zoneMatrix array
   * @param zoneB The zone B index in zoneMatrix array
   * @private
   */
  private areZonesTouched(zoneA: number, zoneB: number): boolean {
    if (zoneA >= this.zoneMatrix.length || zoneB >= this.zoneMatrix.length) {
      return false;
    }
    return this.zoneMatrix[zoneA][zoneB] === 1;
  }

  /**
   * Build an array of years
   * @param yearInterval Object that contains the start year and the end.
   * @param months The month to include in each year.
   * @private
   */
  private buildVegetableTimeLine(
    yearInterval: { start: number; end: number; interval: () => number },
    months: number[]
  ): TimeLine {
    const years = [...Array(yearInterval.interval() + 2).keys()]
      .slice(1)
      .map(x => x + yearInterval.start - 1);
    const object: TimeLine = new Object({}) as TimeLine;
    for (const year of years) {
      object[year] = {};
      for (const month of months) {
        object[year][month] = [];
      }
    }
    return object;
  }

  /**
   * Generates the matrix according to the areas that touch each other.
   * @param zones Fabric Groups.
   * @return matrix of zones.
   */
  private generateZoneMatrix(zones: fabric.Group[]) {
    const newZones = this.sort(zones, 'ascending', 'name'); // Sort zone name by alphabetical order
    const newZoneNames = this.zoneNames.sort();

    const matrix = Array.from(Array(newZones.length), () =>
      Array.from(Array(newZones.length))
    ); // Create an empty matrix.
    for (let i = 0; i < matrix.length; i++) {
      for (let y = 0; y < matrix.length; y++) {
        let value = 0;

        if (newZones[i] !== newZones[y]) {
          if (newZones[i].intersectsWithObject(newZones[y])) {
            value = 1; // Touched !
          }
        }
        matrix[i][y] = value;
      }
    }
    if (newZoneNames) {
      this.showMatrix(matrix, newZoneNames);
    } else {
      this.showMatrix(matrix);
    }
    return matrix;
  }

  /**
   * Sorts an array of objects based on a specified property and order.
   *
   * @param {any[]} array - The array to be sorted.
   * @param {'ascending' | 'descending'} order - The order in which to sort the array.
   * Valid values are 'ascending' and 'descending'.
   * @param {string} property - The property used to determine the order of the objects in the array.
   * @returns {any[]} - A new array containing the sorted objects.
   */
  private sort(
    array: object[],
    order: 'ascending' | 'descending',
    property: string
  ): any[] {
    function compare(a: object, b: object) {
      if (a[property] < b[property]) {
        return order === 'ascending' ? -1 : 1;
      }
      if (a[property] > b[property]) {
        return order === 'ascending' ? 1 : -1;
      }
      return 0;
    }

    return array.sort(compare);
  }

  /**
   * Print a matrix in the console, only for debug.
   * @param value The values of the matrix
   * @param columnName The name associate to each value
   * @private
   */
  private showMatrix(
    value: Array<number | Array<number>>,
    columnName?: string[]
  ) {
    const emptySting = '             ';
    const margin = columnName ? '    ' : '   ';
    const marginXL = columnName ? '     ' : ' ';
    if (columnName) {
      // Log top column name
      const names = String(
        columnName.map((name: string) =>
          (name + emptySting).slice(
            0,
            name.length + 1 + (margin.length - name.length)
          )
        )
      ).replace(/,/g, ''); // Remove ','
      console.log(marginXL, names);
    }
    for (let i = 0; i < value.length; i++) {
      if (columnName) {
        console.log(
          columnName[i],
          emptySting.slice(0, marginXL.length - columnName[i].length),
          String(value[i]).replace(/,/g, margin)
        );
      } else {
        console.log(
          emptySting.slice(0, marginXL.length),
          String(value[i]).replace(/,/g, margin)
        );
      }
    }
  }
}
